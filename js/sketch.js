var correct = "none";
var player = 1;
var score_1 = 0;
var score_2 = 0;
var score_3 = 0;
var score_4 = 0;
var score_5 = 0;
var score_6 = 0;
var x = "Absolute location";
var y = "Absolute humidity";
var z = "Accessibility";
var already_picked = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var question1 = "this isn't right";
var answer_1 = "answer";
var answer_2 = "answer";
var answer_3 = "answer";
var answer_4 = "answer";
var answer_gen = 0;
var question;
var player1 = 1;
var player2 = 12;
var player3 = 21;
var player4 = 32;
var question = 0;

function random() {
    if (isFilled()) {
        already_picked = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        return false;
    }
    question = Math.floor((Math.random() * 27) + 1);
    if (question == 1) {
        if (already_picked[0] == 1) {
            return false;
        } else {
            already_picked[0] = 1;
            question1 = "Who did the native americans side width during the French and Indian war?";
            answer_1 = "What war?";
            answer_2 = "Both sides";
            answer_3 = "None";
            answer_4 = "It depends";
            correct = "answer_4";
        }
    }
    if (question == 2) {
        if (already_picked[1] == 1) {
            return false;
        } else {
            already_picked[1] = 1;
            question1 = "Were the Native Americans enslaved by white colonists in the 18th century?";
            answer_1 = "";
            answer_2 = "Yes";
            answer_3 = "No";
            answer_4 = "";
            correct = "answer_2";
        }
    }

    if (question == 3) {
        if (already_picked[2] == 1) {
            return false;
        } else {
            already_picked[2] = 1;
            question1 = "Did the Native Americans have the same rights as the colonists in the 18th century?";
            answer_1 = "";
            answer_2 = "No";
            answer_3 = "Yes";
            answer_4 = "";
            correct = "answer_2";
        }
    }

    if (question == 4) {
        if (already_picked[3] == 1) {
            return false;
        } else {
            already_picked[3] = 1;
            question1 = "Did the African Americans contribute in the revolutionary war?";
            answer_1 = "";
            answer_2 = "Yes";
            answer_3 = "No";
            answer_4 = "";
            correct = "answer_2";
        }
    }

    if (question == 5) {
        if (already_picked[4] == 1) {
            return false;
        } else {
            already_picked[4] = 1;
            question1 = "Did other African American tribes sell off their prisoners for slave labor?";
            answer_1 = "";
            answer_2 = "Yes";
            answer_3 = "No";
            answer_4 = "";
            correct = "answer_2";
        }
    }

    if (question == 6) {
        if (already_picked[5] == 1) {
            return false;
        } else {
            already_picked[5] = 1;
            question1 = "What fraction did an African American count for voting?";
            answer_1 = "1";
            answer_2 = "2/3";
            answer_3 = "3/5";
            answer_4 = "1/2";
            correct = "answer_3";
        }
    }

    if (question == 7) {
        if (already_picked[6] == 1) {
            return false;
        } else {
            already_picked[6] = 1;
            question1 = "Were women allowed to vote in the 18th century?";
            answer_1 = "";
            answer_2 = "Yes";
            answer_3 = "No";
            answer_4 = "";
            correct = "answer_3";
        }
    }

    if (question == 8) {
        if (already_picked[7] == 1) {
            return false;
        } else {
            already_picked[7] = 1;
            question1 = "Were women seen as objects in the 18th century?";
            answer_1 = "";
            answer_2 = "No";
            answer_3 = "Yes";
            answer_4 = "";
            correct = "answer_3";
        }
    }
    if (question == 9) {
        if (already_picked[8] == 1) {
            return false;
        } else {
            already_picked[8] = 1;
            question1 = "Did the constitution allow women to have the same rights that men did when it was first created?";
            answer_1 = "";
            answer_2 = "Yes";
            answer_3 = "No";
            answer_4 = "";
            correct = "answer_3";
        }
    }
    if (question == 10) {
        if (already_picked[9] == 1) {
            return false;
        } else {
            already_picked[9] = 1;
            question1 = "What was the name of the trail that the Native Americans were forced to travel?";
            answer_1 = "Trail of Tears";
            answer_2 = "Trail of Sadness";
            answer_3 = "Trail of Blood";
            answer_4 = "Trail of Suffering";
            correct = "answer_1";
        }
    }

    if (question == 11) {
        if (already_picked[10] == 1) {
            return false;
        } else {
            already_picked[10] = 1;
            question1 = "Who led a group of Native Americans on a retreat throughout the north west?";
            answer_1 = "Cheif Tool-hool-hool-suite";
            answer_2 = "Rolling Stone";
            answer_3 = "Chief Joseph";
            answer_4 = "Chief Bear";
            correct = "answer_3";
        }
    }

    if (question == 12) {
        if (already_picked[11] == 1) {
            return false;
        } else {
            already_picked[11] = 1;
            question1 = "What is the name of the land that the Native Americans are forced to live on?";
            answer_1 = "Boundaries";
            answer_2 = "Mediations";
            answer_3 = "Conservations";
            answer_4 = "Reservations";
            correct = "answer_4";
        }
    }

    if (question == 13) {
        if (already_picked[12] == 1) {
            return false;
        } else {
            already_picked[12] = 1;
            question1 = "When did African Americans gain the right to vote?";
            answer_1 = "1868";
            answer_2 = "1834";
            answer_3 = "1876";
            answer_4 = "1854";
            correct = "answer_1";
        }
    }

    if (question == 14) {
        if (already_picked[13] == 1) {
            return false;
        } else {
            already_picked[13] = 1;
            question1 = "What was the court case that deemed it that African Americans had no rights?";
            answer_1 = "Dred Scott v Supreme Court";
            answer_2 = "Dred Scott v Constitution";
            answer_3 = "African Americans v Supreme Court";
            answer_4 = "African Americans voting act";
            correct = "answer_1";
        }
    }
    if (question == 15) {
        if (already_picked[14] == 1) {
            return false;
        } else {
            already_picked[14] = 1;
            question1 = "What was the name of the law that forced notherners to capture freed slaves?";
            answer_1 = "Capture laws";
            answer_2 = "Fugitive Slave Act";
            answer_3 = "No Man Left Behind";
            answer_4 = "Capture Act";
            correct = "answer_2";
        }
    }

    if (question == 16) {
        if (already_picked[15] == 1) {
            return false;
        } else {
            already_picked[15] = 1;
            question1 = "Is republican motherhood the same as feminism?";
            answer_1 = "";
            answer_2 = "No";
            answer_3 = "Yes";
            answer_4 = "";
            correct = "answer_2";
        }
    }

    if (question == 17) {
        if (already_picked[16] == 1) {
            return false;
        } else {
            already_picked[16] = 1;
            question1 = "Which feminist during the 18th century wanted legal prostitution?";
            answer_1 = "Angelina Grimke";
            answer_2 = "Lucretia Mott";
            answer_3 = "Susan B Anthony";
            answer_4 = "Mary Wollstonecraft";
            correct = "answer_4";
        }
    }
    if (question == 18) {
        if (already_picked[17] == 1) {
            return false;
        } else {
            already_picked[17] = 1;
            question1 = "Who won the right for women to own property in New York?";
            answer_1 = "Susan B Anthony";
            answer_2 = "Sarah Grimke";
            answer_3 = "Elizabeth Staton";
            answer_4 = "Martha Ballard";
            correct = "answer_1";
        }
    }
    if (question == 19) {
        if (already_picked[18] == 1) {
            return false;
        } else {
            already_picked[18] = 1;
            question1 = "When did the Japanese government surrender during WWII?";
            answer_1 = "1945";
            answer_2 = "1932";
            answer_3 = "1981";
            answer_4 = "1946";
            correct = "answer_1";
        }
    }

    if (question == 20) {
        if (already_picked[19] == 1) {
            return false;
        } else {
            already_picked[19] = 1;
            question1 = "Were the japanese seen as enemies during parts of the 19th century?";
            answer_1 = "";
            answer_2 = "Yes";
            answer_3 = "No";
            answer_4 = "";
            correct = "answer_2";
        }
    }

    if (question == 21) {
        if (already_picked[20] == 1) {
            return false;
        } else {
            already_picked[20] = 1;
            question1 = "Were the japanese forced into camps similar to the jews in WWII?";
            answer_1 = "";
            answer_2 = "Yes";
            answer_3 = "No";
            answer_4 = "";
            correct = "answer_2";
        }
    }

    if (question == 22) {
        if (already_picked[21] == 1) {
            return false;
        } else {
            already_picked[21] = 1;
            question1 = "Were African Americans segregated even though they were free?";
            answer_1 = "";
            answer_2 = "No";
            answer_3 = "Yes";
            answer_4 = "";
            correct = "answer_3";
        }
    }

    if (question == 23) {
        if (already_picked[22] == 1) {
            return false;
        } else {
            already_picked[22] = 1;
            question1 = "How many African Americans lived in the US during the earlier 1900's?";
            answer_1 = "5 million";
            answer_2 = "10 million";
            answer_3 = "8 million";
            answer_4 = "7 million";
            correct = "answer_2";
        }
    }

    if (question == 24) {
        if (already_picked[23] == 1) {
            return false;
        } else {
            already_picked[23] = 1;
            question1 = "When did the Harlem Renaissance take place?";
            answer_1 = "1950's";
            answer_2 = "1970's";
            answer_3 = "1920's";
            answer_4 = "1940's";
            correct = "answer_3";
        }
    }

    if (question == 25) {
        if (already_picked[24] == 1) {
            return false;
        } else {
            already_picked[24] = 1;
            question1 = "What amendment gave women the right to vote?";
            answer_1 = "15th";
            answer_2 = "19th";
            answer_3 = "23rd";
            answer_4 = "21st";
            correct = "answer_2";
        }
    }
    if (question == 26) {
        if (already_picked[25] == 1) {
            return false;
        } else {
            already_picked[25] = 1;
            question1 = "Did women make more money than men in the 1900's?";
            answer_1 = "";
            answer_2 = "Yes";
            answer_3 = "No";
            answer_4 = "";
            correct = "answer_3";
        }
    }
    if (question == 27) {
        if (already_picked[26] == 1) {
            return false;
        } else {
            already_picked[26] = 1;
            question1 = "What proposed amendment tried to give equals rights to all regardless of sex?";
            answer_1 = "Free Rights Amendment";
            answer_2 = "Equal Rights Amendment";
            answer_3 = "Justice For All Amendment";
            answer_4 = "Equality Amendment";
            correct = "answer_2";
        }
    }
    console.log(already_picked);
    console.log(question1);
    console.log(answer_1);
    console.log(answer_2);
    console.log(answer_3);
    console.log(answer_4);
    document.getElementById("question").innerHTML = question1;
    document.getElementById("answer_1").innerHTML = answer_1;
    document.getElementById("answer_2").innerHTML = answer_2;
    document.getElementById("answer_3").innerHTML = answer_3;
    document.getElementById("answer_4").innerHTML = answer_4;
    return true;
}

function isFilled() {
    for (i = 0; i < already_picked.length; i++)
        if (already_picked[i] == 0)
            return false;
    return true;
}

function submit_1() {
    if (correct == "answer_1") {
        document.getElementById("correct_incorect").innerHTML = "Correct!";
        if (player == 1) {
            document.getElementById(player1).style.backgroundColor = "#cacaca";
            player1 += 1;
            score_1 += 10;
        }
        if (player == 2) {
            document.getElementById(player2).style.backgroundColor = "#cacaca";
            player2 += 1;
            score_2 += 10;
        }
        if (player == 3) {
            document.getElementById(player3).style.backgroundColor = "#cacaca";
            player3 += 1;
            score_3 += 10;
        }
        if (player == 4) {
            document.getElementById(player4).style.backgroundColor = "#cacaca";
            player4 += 1;
            score_4 += 10;
        }
    } else {
        document.getElementById("correct_incorect").innerHTML = "incorrect :(";
        if (player == 1) {
            score_1 -= 10;

        }
        if (player == 2) {
            score_2 -= 10;
        }
        if (player == 3) {
            score_3 -= 10;
        }
        if (player == 4) {
            score_4 -= 10;
        }
        if (player == 5) {
            score_5 -= 10;
        }
        if (player == 6) {
            score_6 -= 10;
        }
    }
    document.getElementById("score_1").innerHTML = score_1;
    document.getElementById("score_2").innerHTML = score_2;
    document.getElementById("score_3").innerHTML = score_3;
    document.getElementById("score_4").innerHTML = score_4;
    checkplayer();

}

function submit_2() {
    if (correct == "answer_2") {
        document.getElementById("correct_incorect").innerHTML = "Correct!";
        if (player == 1) {
            document.getElementById(player1).style.backgroundColor = "#cacaca";
            player1 += 1;
            score_1 += 10;
        }
        if (player == 2) {
            document.getElementById(player2).style.backgroundColor = "#cacaca";
            player2 += 1;
            score_2 += 10;
        }
        if (player == 3) {
            document.getElementById(player3).style.backgroundColor = "#cacaca";
            player3 += 1;
            score_3 += 10;
        }
        if (player == 4) {
            document.getElementById(player4).style.backgroundColor = "#cacaca";
            player4 += 1;
            score_4 += 10;
        }
    } else {
        document.getElementById("correct_incorect").innerHTML = "incorrect :(";
        if (player == 1) {
            score_1 -= 10;
        }
        if (player == 2) {
            score_2 -= 10;
        }
        if (player == 3) {
            score_3 -= 10;
        }
        if (player == 4) {
            score_4 -= 10;
        }
        if (player == 5) {
            score_5 -= 10;
        }
        if (player == 6) {
            score_6 -= 10;
        }
    }
    document.getElementById("score_1").innerHTML = score_1;
    document.getElementById("score_2").innerHTML = score_2;
    document.getElementById("score_3").innerHTML = score_3;
    document.getElementById("score_4").innerHTML = score_4;
    checkplayer();
}

function submit_3() {
    if (correct == "answer_3") {
        document.getElementById("correct_incorect").innerHTML = "Correct!";
        if (player == 1) {
            document.getElementById(player1).style.backgroundColor = "#cacaca";
            player1 += 1;
            score_1 += 10;
        }
        if (player == 2) {
            document.getElementById(player2).style.backgroundColor = "#cacaca";
            player2 += 1;
            score_2 += 10;
        }
        if (player == 3) {
            document.getElementById(player3).style.backgroundColor = "#cacaca";
            player3 += 1;
            score_3 += 10;
        }
        if (player == 4) {
            document.getElementById(player4).style.backgroundColor = "#cacaca";
            player4 += 1;
            score_4 += 10;
        }
    } else {
        document.getElementById("correct_incorect").innerHTML = "incorrect :(";
        if (player == 1) {
            score_1 -= 10;
        }
        if (player == 2) {
            score_2 -= 10;
        }
        if (player == 3) {
            score_3 -= 10;
        }
        if (player == 4) {
            score_4 -= 10;
        }
        if (player == 5) {
            score_5 -= 10;
        }
        if (player == 6) {
            score_6 -= 10;
        }
    }
    document.getElementById("score_1").innerHTML = score_1;
    document.getElementById("score_2").innerHTML = score_2;
    document.getElementById("score_3").innerHTML = score_3;
    document.getElementById("score_4").innerHTML = score_4;
    checkplayer();
}

function submit_4() {

    if (correct == "answer_4") {
        document.getElementById("correct_incorect").innerHTML = "Correct!";
        if (player == 1) {
            document.getElementById(player1).style.backgroundColor = "#cacaca";
            player1 += 1;
            score_1 += 10;
        }
        if (player == 2) {
            document.getElementById(player2).style.backgroundColor = "#cacaca";
            player2 += 1;
            score_2 += 10;
        }
        if (player == 3) {
            document.getElementById(player3).style.backgroundColor = "#cacaca";
            player3 += 1;
            score_3 += 10;
        }
        if (player == 4) {
            document.getElementById(player4).style.backgroundColor = "#cacaca";
            player4 += 1;
            score_4 += 10;
        }
    } else {
        document.getElementById("correct_incorect").innerHTML = "incorrect :(";
        if (player == 1) {
            score_1 -= 10;
        }
        if (player == 2) {
            score_2 -= 10;
        }
        if (player == 3) {
            score_3 -= 10;
        }
        if (player == 4) {
            score_4 -= 10;
        }
    }
    document.getElementById("score_1").innerHTML = score_1;
    document.getElementById("score_2").innerHTML = score_2;
    document.getElementById("score_3").innerHTML = score_3;
    document.getElementById("score_4").innerHTML = score_4;
    checkplayer();

}

function checkplayer() {
    if (player >= 4) {
        player = 1;
    } else {
        player += 1;
    }
    document.getElementById("playerid").innerHTML = "Player " + player + "'s turn";
    while (!random()) {
        console.log("oof");
    }
    run_player1();
    run_player2();
    run_player3();
    run_player4();
}

function init() {

    random();
    checkplayer();
    player_alert();
}

function run_player1() {

    document.getElementById(player1 - 1).style.backgroundColor = "#cacaca";
    if (player1 > 36) {
        player1 = 1;
    }
    document.getElementById(player1).style.backgroundColor = "orange";
    console.log(player1);
    if (score_1 >= 50) {
        alert("Player 1 Wins!");
    }

}


function run_player2() {

    document.getElementById(player2 - 1).style.backgroundColor = "#cacaca";
    if (player2 > 36) {
        player2 = 1;
    }
    document.getElementById(player2).style.backgroundColor = "#610b0b";
    console.log(player2);
    if (score_2 >= 50) {
        alert("Player 2 Wins!");
    }

}

function run_player3() {

    document.getElementById(player3 - 1).style.backgroundColor = "#cacaca";
    if (player3 > 36) {
        player3 = 1;
    }
    document.getElementById(player3).style.backgroundColor = "#ff00ff";


    if (score_3 >= 50) {
        alert("Player 3 Wins!");
    }

}

function run_player4() {

    document.getElementById(player4 - 1).style.backgroundColor = "#cacaca";
    if (player4 > 36) {
        player4 = 1;
    }
    document.getElementById(player4).style.backgroundColor = "#00ff00";

    if (score_4 >= 50) {
        alert("Player 4 Wins!");
    }
}


function player_alert() {
    alert("Welcome to History^3, where you get asked history questions relating to the 18th through 20th centuries while running around a cube.");
    alert("Players: \nOrange - Player 1\nRed - Player 2\nPink - Player 3\nGreen - Player 4");
    alert("Win condition : First player to 50 points wins!\nYou get 10 points for getting a question right, and -10 for getting it wrong! Good luck!");
}